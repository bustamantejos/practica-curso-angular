import { Injectable } from '@angular/core';

import {Observable} from "rxjs";
import { LoginObject } from '../models/loginObject';
import { Session } from '../models/session';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  token = "ijJq2dvDs3QWxPk1Dt3r";
  session : Session = null;
  user = new User();
  constructor(private http: HttpClient) {}
  
  //todo
  private basePath = '/api/authenticate/';

 login(loginObj: LoginObject): Session {
   //como no tenemos un servicio de autenticación asumimos que el login ha sido correcto
   //hardcodeando el token de acceso a la API
   this.user.username = loginObj.username;
   this.user.name = loginObj.username;
   this.session = {"token":"ijJq2dvDs3QWxPk1Dt3r","user":this.user} 

  return this.session;
 }

 logout(){
  this.session = null;
  this.user = null;
 }



}
