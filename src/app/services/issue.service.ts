import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Issue } from '../models/issue';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  urlApi = `https://gitlab.com/api/v4/issues`;
  
  
  constructor(private http: HttpClient, private storageService: StorageService) {

   }

  ngOnInit(){
    this.getIssues();
  }

  getIssues() {
    const headers = new HttpHeaders().set("PRIVATE-TOKEN", this.storageService.getCurrentToken());
    return this.http.get<Issue[]>(this.urlApi, {headers});
  }

  getIssue(project_id:string, iid:string) {

    const headers = new HttpHeaders().set("PRIVATE-TOKEN", this.storageService.getCurrentToken());
    return this.http.get<Issue[]>(`https://gitlab.com/api/v4/projects/${project_id}/issues/${iid}`, {headers});

  }
    

}
