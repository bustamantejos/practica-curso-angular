import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Issue } from 'src/app/models/issue';
import { catchError, map, tap } from 'rxjs/operators';
import { IssueService } from 'src/app/services/issue.service';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit {

  items = [];
  
  constructor(private issueService: IssueService) {

   }

  ngOnInit(){
    this.getIssues();
  }

  getIssues(): void {
    
    this.issueService.getIssues().subscribe(
      res => {this.items = res ; console.log(this.items)},
      err => console.log('HTTP Error', err),
      () => console.log('HTTP request completed.')
    ); 
    
  }

}
