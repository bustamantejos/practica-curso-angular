import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Issue } from 'src/app/models/issue';
import { IssueService } from 'src/app/services/issue.service';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {

  issue : any;
  issue_id: string;
  project_id : string;

  constructor(route: ActivatedRoute, private issueService: IssueService) { 
    this.issue_id = route.snapshot.paramMap.get('iid');
    this.project_id = route.snapshot.paramMap.get('project_id');
  }

  ngOnInit(): void {
    this.getIssueDetail();
  }


  getIssueDetail(): void {
    
    this.issueService.getIssue(this.project_id, this.issue_id).subscribe(
      res => {this.issue = res ; console.log(this.issue)},
      err => console.log('HTTP Error', err),
      () => console.log('HTTP request completed.')
    ); 
    
  }

}
