import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup} from "@angular/forms";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Issue } from 'src/app/models/issue';
import { catchError, map, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { StorageService } from 'src/app/services/storage.service';
import { Router } from '@angular/router';
import { LoginObject } from 'src/app/models/loginObject';
import { Session } from 'src/app/models/session';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public submitted: Boolean = false;
  public error: {code: number, message: string} = null;
  public fakeSess : Session;
  //urlApi = `https://gitlab.com/api/v4/issues`

  constructor( private authenticationService: AuthenticationService,
               private storageService: StorageService,
               private router: Router) {

   }

  ngOnInit(){
   
  }

  public submitLogin(values): void {
    this.submitted = true;
    this.setFakeLogin(this.authenticationService.login(new LoginObject(values)));
  }

  private setFakeLogin(data: Session){
    this.storageService.setCurrentSession(data);
    this.router.navigate(['/home']);
  }


}
