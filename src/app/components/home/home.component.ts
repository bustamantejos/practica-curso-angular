import { Component, OnInit } from '@angular/core';
import { IssueService } from 'src/app/services/issue.service';
import { User } from 'src/app/models/user';
import { StorageService } from 'src/app/services/storage.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public user: User;

  constructor(private storageService: StorageService,
              private authenticationService: AuthenticationService,
              private issueService: IssueService) { }

  ngOnInit(): void {
    this.user = this.storageService.getCurrentUser();
    /*this.issueService.getIssues().subscribe((data: any) => {
      console.log(data);
    });*/

  }

  public logout(): void{
    this.authenticationService.logout();    
  }

}
