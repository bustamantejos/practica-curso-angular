export class Issue {
    id: number;
    iid: number;
    due_date: string;
    project_id: number;
    title: string;
    state: string;
    labels: string;
    with_labels_details : boolean;
    milestone: string;
    scope: string;
    author_id: number;
    author_username: string;
    assignee_id: number;
    assignee_username : string;
    weight : number;
  }