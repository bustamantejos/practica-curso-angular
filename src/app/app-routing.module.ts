import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssuesComponent } from './components/issues/issues.component';
import { IssueDetailComponent } from './components/issue-detail/issue-detail.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AuthorizedGuard } from './guards/authorized.guard';


const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [ AuthorizedGuard ] },
  { path: 'issues', component: IssuesComponent, canActivate: [ AuthorizedGuard ] },
  { path: 'issue/:iid/:project_id', component: IssueDetailComponent, canActivate: [ AuthorizedGuard ]},
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
